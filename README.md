# A

## Content

```
./A. Belski & E. Golistrom & Z. Kabys & Gh. Chiriac & O. Kobeakova:
A. Belski & E. Golistrom & Z. Kabys & Gh. Chiriac & O. Kobeakova - Enciclopedia intelepciunii.pdf

./A. K. Larionov:
A. K. Larionov - Geologia distractiva.pdf

./A. Migdal:
A. Migdal - De la indoiala la certitudine.pdf

./A. P. Iuschevici:
A. P. Iuschevici - Istoria matematicii in evul mediu.pdf

./Abdur Rahman:
Abdur Rahman - Anatomia stiintei.pdf

./Adam Smith:
Adam Smith - Avutia natiunilor.pdf

./Adrian Miroiu:
Adrian Miroiu - Teorii ale dreptatii.pdf

./Adrian Nita:
Adrian Nita - Leibniz.pdf
Adrian Nita - Noica, o filosofie a individualitatii.pdf

./Adrian Paul Iliescu:
Adrian Paul Iliescu - Cunoastere si analiza.pdf
Adrian Paul Iliescu - Etica sociala si politica.pdf
Adrian Paul Iliescu - Filosofia sociala a lui F. A. von Hayek.pdf

./Adrian Paul Iliescu & Mihai Radu Solcan:
Adrian Paul Iliescu & Mihai Radu Solcan - Limitele puterii.pdf

./Al. Valeriu:
Al. Valeriu - Logica.pdf

./Alain Besancon:
Alain Besancon - Imaginea interzisa.pdf

./Alain de Botton:
Alain de Botton - Consolarile filosofiei.pdf
Alain de Botton - Despre farmecul lucrurilor plictisitoare.pdf
Alain de Botton - Eseuri de indragostit.pdf
Alain de Botton - Sex, shopping si un roman.pdf

./Alain de Libera:
Alain de Libera - Cearta universaliilor.pdf
Alain de Libera - Gandirea Evului Mediu.pdf

./Alain Graf:
Alain Graf - Lexic de filosofie.pdf
Alain Graf - Marii filosofi contemporani.pdf

./Alan Montefiore:
Alan Montefiore - Introducere moderna in filozofia moralei.pdf

./Alan Montefiore & Valentin Muresan:
Alan Montefiore & Valentin Muresan - Filosofia morala britanica.pdf

./Alasdair MacIntyre:
Alasdair MacIntyre - Tratat de morala (Dupa virtute).pdf

./Albert Camus & Althur Koestler:
Albert Camus & Althur Koestler - Reflectii asupra pedepsei cu moartea.pdf

./Albert cel Mare:
Albert cel Mare - Despre destin.pdf

./Albert Einstein:
Albert Einstein - Cum vad eu lumea.pdf
Albert Einstein - Evolutia fizicii.pdf
Albert Einstein - Teoria relativitatii.pdf

./Albert Szent Gyorgyi:
Albert Szent Gyorgyi - Pledoarie pentru viata.pdf

./Alexander Baumgarten:
Alexander Baumgarten - Sfantul Anselm si conceptul ierarhiei.pdf

./Alexander Baumgarten & Vasile Musca:
Alexander Baumgarten & Vasile Musca - Filosofia politica a lui Platon.pdf

./Alexandre Kojeve:
Alexandre Kojeve - De la lumea inchisa la universul infinit.pdf
Alexandre Kojeve - Filozofia lui Jakob Bohme.pdf
Alexandre Kojeve - Introducere in lectura lui Hegel.pdf

./Alexandru Boboc:
Alexandru Boboc - Filosofi Contemporani (II).pdf
Alexandru Boboc - Studii de istorie a filosofiei universale XXI.pdf

./Alexandru Boboc & Mircea Flonta:
Alexandru Boboc & Mircea Flonta - Immanuel Kant (200 de ani de la aparitia Criticii ratiunii pure).pdf

./Alexandru Boboc & Sergiu Balan:
Alexandru Boboc & Sergiu Balan - Studii de teoria categoriilor, vol. 5.pdf

./Alexandru Petrescu:
Alexandru Petrescu - Lucian Blaga, o noua paradigma in filosofia stiintei.pdf

./Alexandru Posescu:
Alexandru Posescu - Introducere in filosofie (1939).pdf

./Alexandru Surdu:
Alexandru Surdu - Elemente de logica intuitionista.pdf
Alexandru Surdu - Filosofia moderna.pdf
Alexandru Surdu - Gandirea speculativa.pdf
Alexandru Surdu - Istoria logicii romanesti.pdf
Alexandru Surdu - Logica clasica si logica matematica.pdf

./Alexandru Surdu & Sergiu Balan:
Alexandru Surdu & Sergiu Balan - Studii de teoria categoriilor, vol. 1.pdf
Alexandru Surdu & Sergiu Balan - Studii de teoria categoriilor, vol. 2.pdf
Alexandru Surdu & Sergiu Balan - Studii de teoria categoriilor, vol. 3.pdf
Alexandru Surdu & Sergiu Balan - Studii de teoria categoriilor, vol. 4.pdf

./Alfred Jules Ayer:
Alfred Jules Ayer - Hume (O scurta introducere).pdf

./Alfred Kastler:
Alfred Kastler - Aceasta stranie materie.pdf

./Algirdas Julien Greimas:
Algirdas Julien Greimas - Despre sens.pdf

./Allah:
Allah - Coranul cel sfant.pdf

./Allan Janik & Stephen Toulmin:
Allan Janik & Stephen Toulmin - Viena lui Wittgenstein.pdf

./Alvin Plantinga:
Alvin Plantinga - Natura necesitatii.pdf

./Alvin Toffler:
Alvin Toffler - Al treilea val.pdf
Alvin Toffler - Consumatorii de cultura.pdf
Alvin Toffler - Socul viitorului.pdf

./Amos Funkenstein:
Amos Funkenstein - Teologie si imaginatia stiintifica.pdf

./Ana Felicia Stef:
Ana Felicia Stef - Manual de greaca veche.pdf

./Andre Comte Sponville:
Andre Comte Sponville - Mic tratat al marilor virtuti.pdf

./Andre Gide:
Andre Gide - Jurnal.pdf

./Andrei Cornea:
Andrei Cornea - Cand Socrate nu are dreptate.pdf
Andrei Cornea - De la Scoala din Atena la Scoala de la Paltinis.pdf
Andrei Cornea - Noul (O veche poveste).pdf
Andrei Cornea - O istorie a nefiintei in filozofia greaca.pdf
Andrei Cornea - Platon. Filozofie si cenzura.pdf
Andrei Cornea - Turnirul khazar.pdf

./Andrei Marga:
Andrei Marga - Filosofia americana, vol. 1.pdf

./Andrei Oisteanu:
Andrei Oisteanu - Narcotice in cultura romana.pdf

./Andrei Plesu:
Andrei Plesu - Minima Moralia.pdf
Andrei Plesu - Parabolele lui Iisus.pdf

./Andre Vergez & Denis Huisman:
Andre Vergez & Denis Huisman - Curs de filosofie.pdf

./Angela Botez:
Angela Botez - Filosofia mentalului (Intentionalitate si experiment).pdf

./Anne Cheng:
Anne Cheng - Istoria gandirii chineze.pdf

./Anselm:
Anselm - Monologion (Apostrof).pdf
Anselm - Proslogion (Apostrof).pdf

./Anselm de Canterbury:
Anselm de Canterbury - De ce s-a facut Dumnezeu om.pdf
Anselm de Canterbury - Despre libertatea alegerii.pdf
Anselm de Canterbury - Proslogion.pdf

./Anthony Clifford Grayling:
Anthony Clifford Grayling - Russell. O scurta introducere.pdf
Anthony Clifford Grayling - Wittgenstein (Maestrii spiritului).pdf

./Anthony Gottlieb:
Anthony Gottlieb - Socrate, filosoful martir.pdf

./Anthony Kenny:
Anthony Kenny - Toma d'Aquino (Maestrii spiritului).pdf

./Anthony Stevens:
Anthony Stevens - Jung (Maestrii spiritului).pdf

./Anton Dumitriu:
Anton Dumitriu - Culturi eleate si culturi heraclitice.pdf
Anton Dumitriu - Homo universalis.pdf
Anton Dumitriu - Istoria logicii.pdf
Anton Dumitriu - Logica polivalenta.pdf
Anton Dumitriu - Philosophia mirabilis.pdf
Anton Dumitriu - Solutia paradoxelor logico-matematice.pdf
Anton Dumitriu - Teoria logicii.pdf

./Anton Hugli & Poul Lubcke:
Anton Hugli & Poul Lubcke - Filosofia in secolul XX, vol. 1.pdf
Anton Hugli & Poul Lubcke - Filosofia in secolul XX, vol. 2.pdf

./Antony Flew:
Antony Flew - Dictionar de filosofie si logica.pdf

./Aram M. Frenkian:
Aram M. Frenkian - Scepticismul grec.pdf

./Arend Lijphart:
Arend Lijphart - Modele ale democratiei.pdf

./Aristotel:
Aristotel - (Pseudo) Liber de causis.pdf
Aristotel - Categorii, Despre interpretare (Humanitas).pdf
Aristotel - De anima. Parva naturalia.pdf
Aristotel - Despre cer.pdf
Aristotel - Despre generare si nimicire.pdf
Aristotel - Despre suflet (Humanitas).pdf
Aristotel - Etica Nicomahica.pdf
Aristotel - Fizica.pdf
Aristotel - Metafizica.pdf
Aristotel - Organon I.pdf
Aristotel - Organon II.pdf
Aristotel - Poetica.pdf
Aristotel - Politica (IRI).pdf
Aristotel - Politica.pdf
Aristotel - Retorica.pdf

./Aristotel & Plotini:
Aristotel & Plotini - Despre eternitatea lumii.pdf
Aristotel & Plotini - Despre unitatea intelectului.pdf

./Arthur Bloch:
Arthur Bloch - Legea lui Murphy.pdf

./Arthur Koestler:
Arthur Koestler - Lunaticii.pdf

./Arthur Oncken Lovejoy:
Arthur Oncken Lovejoy - Marele lant al fiintei.pdf

./Arthur Schopenhauer:
Arthur Schopenhauer - Aforisme asupra intelepciunii in viata.pdf
Arthur Schopenhauer - Despre impatrita radacina a principiului ratiunii suficiente.pdf
Arthur Schopenhauer - Eseu despre liberul arbitru.pdf
Arthur Schopenhauer - Fundamentele Moralei.pdf
Arthur Schopenhauer - Lumea ca vointa si reprezentare, vol. 1 (Humanitas).pdf
Arthur Schopenhauer - Lumea ca vointa si reprezentare, vol. 2 (Humanitas).pdf
Arthur Schopenhauer - Lumea ca vointa si reprezentare, vol. I.pdf
Arthur Schopenhauer - Lumea ca vointa si reprezentare, vol. II.pdf
Arthur Schopenhauer - Lumea ca vointa si reprezentare, vol. III.pdf
Arthur Schopenhauer - Parerga si paralipomena.pdf

./Auguste Comte:
Auguste Comte - Discurs asupra spiritului pozitiv.pdf

./Augustin:
Augustin - Confesiuni.pdf
Augustin - De dialectica.pdf
Augustin - De Magistro.pdf
Augustin - Despre adevarata religie.pdf
Augustin - Revizuiri.pdf
Augustin - Solilocvii.pdf

./Aurel Cazacu:
Aurel Cazacu - Teoria argumentarii.pdf

./Aurelia Marinescu:
Aurelia Marinescu - Codul bunelor maniere astazi.pdf
```

